package untref.lp1.tp1;

import static untref.lp1.tp1.EstadoCelula.MUERTA;
import static untref.lp1.tp1.EstadoCelula.VIVA;

import org.junit.Assert;
import org.junit.Test;

public class TestsParaCalificarTrabajos {



	@Test
	public void evolucionaTableroVacio() {
		EstadoCelula[][] inicial = { {} };
		JuegoDeLaVida juego = new JuegoDeLaVida(new Tablero(inicial));

		EstadoCelula[][] resultado = { {} };

		Tablero proyeccion = juego.proyectar(1);
		Assert.assertArrayEquals(resultado, proyeccion.getTablero());
	}

	@Test
	public void evolucionarTableroTrivial() {
		EstadoCelula[][] inicial = {
				{VIVA}
			};

		EstadoCelula[][] resultado = {
				{MUERTA}
			};

		JuegoDeLaVida juego = new JuegoDeLaVida(new Tablero(inicial));
		Tablero proyeccion = juego.proyectar(1);
		Assert.assertArrayEquals(resultado, proyeccion.getTablero());
	}

	@Test
	public void evolucionarTableroTodasMuertas() {
		EstadoCelula[][] inicial = {
				{ MUERTA, MUERTA },
				{ MUERTA, MUERTA } };

		JuegoDeLaVida juego = new JuegoDeLaVida(new Tablero(inicial));

		EstadoCelula[][] resultado = {
				{ MUERTA, MUERTA },
				{ MUERTA, MUERTA } };

		Tablero proyeccion = juego.proyectar(1);
		Assert.assertArrayEquals(resultado, proyeccion.getTablero());
	}

	@Test
	public void evolucionarTableroTodasVivas() {
		EstadoCelula[][] inicial = {
				{ VIVA, VIVA },
				{ VIVA, VIVA } };

		JuegoDeLaVida juego = new JuegoDeLaVida(new Tablero(inicial));

		EstadoCelula[][] resultado = {
				{ MUERTA, MUERTA },
				{ MUERTA, MUERTA } };

		Tablero proyeccion = juego.proyectar(1);
		Assert.assertArrayEquals(resultado, proyeccion.getTablero());
	}

	@Test
	public void evolucionarGliderCuatroGeneraciones() {
		EstadoCelula[][] inicial = {
				{MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA},
				{MUERTA, MUERTA,   VIVA, MUERTA, MUERTA, MUERTA, MUERTA},
				{MUERTA, MUERTA, MUERTA,   VIVA, MUERTA, MUERTA, MUERTA},
				{MUERTA,   VIVA,   VIVA,   VIVA, MUERTA, MUERTA, MUERTA},
				{MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA},
				{MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA},
				{MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA},
			};

		EstadoCelula[][] resultado = {
				{MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA},
				{MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA},
				{MUERTA, MUERTA, MUERTA,  VIVA,  MUERTA, MUERTA, MUERTA},
				{MUERTA, MUERTA, MUERTA, MUERTA,   VIVA, MUERTA, MUERTA},
				{MUERTA, MUERTA,   VIVA,   VIVA,   VIVA, MUERTA, MUERTA},
				{MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA},
				{MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA},
			};

		JuegoDeLaVida juego = new JuegoDeLaVida(new Tablero(inicial));
		Tablero proyeccion = juego.proyectar(4);
		Assert.assertArrayEquals(resultado, proyeccion.getTablero());
	}

	@Test
	public void evolucionarEstructuraEstableDiezGeneraciones() {
		EstadoCelula[][] inicial = {
				{MUERTA,   VIVA,   VIVA},
				{MUERTA,   VIVA,   VIVA},
				{MUERTA, MUERTA, MUERTA}
			};

		EstadoCelula[][] resultado = {
				{MUERTA,   VIVA,   VIVA},
				{MUERTA,   VIVA,   VIVA},
				{MUERTA, MUERTA, MUERTA}
			};

		JuegoDeLaVida juego = new JuegoDeLaVida(new Tablero(inicial));
		Tablero proyeccion = juego.proyectar(10);
		Assert.assertArrayEquals(resultado, proyeccion.getTablero());
	}

	@Test
	public void evolucinoarGeneracionAlAzar() {
		EstadoCelula[][] inicial = {
				{MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA},
				{MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA},
				{MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA},
				{MUERTA, MUERTA, MUERTA,   VIVA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA},
				{MUERTA, MUERTA, MUERTA, MUERTA, MUERTA,   VIVA, MUERTA, MUERTA, MUERTA, MUERTA},
				{MUERTA, MUERTA,   VIVA,   VIVA, MUERTA, MUERTA,   VIVA,   VIVA,   VIVA, MUERTA},
				{MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA},
				{MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA},
				{MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA},
				{MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA}
			};

		EstadoCelula[][] resultado = {
				{MUERTA,   VIVA, MUERTA,   VIVA,   VIVA, MUERTA, MUERTA, MUERTA, MUERTA,   VIVA},
				{MUERTA,   VIVA,   VIVA,   VIVA,   VIVA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA},
				{MUERTA,   VIVA, MUERTA, MUERTA, MUERTA,   VIVA,   VIVA, MUERTA, MUERTA, MUERTA},
				{  VIVA,   VIVA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA},
				{MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA,   VIVA, MUERTA, MUERTA, MUERTA},
				{MUERTA, MUERTA, MUERTA, MUERTA,   VIVA, MUERTA,   VIVA, MUERTA, MUERTA, MUERTA},
				{MUERTA,   VIVA,   VIVA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA,   VIVA},
				{MUERTA, MUERTA,   VIVA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA,   VIVA},
				{MUERTA, MUERTA,   VIVA, MUERTA, MUERTA, MUERTA, MUERTA,   VIVA, MUERTA, MUERTA},
				{  VIVA, MUERTA, MUERTA, MUERTA, MUERTA,   VIVA, MUERTA, MUERTA,   VIVA,   VIVA}
			};

		JuegoDeLaVida juego = new JuegoDeLaVida(new Tablero(inicial));
		Tablero proyeccion = juego.proyectar(25);
		Assert.assertArrayEquals(resultado, proyeccion.getTablero());
	}

	@Test
	public void evolucionarEstructuraEstableUnaDiezYCienGeneraciones() {
		EstadoCelula[][] inicial = {
				{MUERTA, MUERTA, MUERTA, MUERTA},
				{MUERTA,   VIVA,   VIVA, MUERTA},
				{MUERTA,   VIVA,   VIVA, MUERTA},
				{MUERTA, MUERTA, MUERTA, MUERTA}
			};

		JuegoDeLaVida juego = new JuegoDeLaVida(new Tablero(inicial));
		Tablero proyeccion = juego.proyectar(1);
		Assert.assertArrayEquals(inicial, proyeccion.getTablero());

		proyeccion = juego.proyectar(10);
		Assert.assertArrayEquals(inicial, proyeccion.getTablero());

		proyeccion = juego.proyectar(100);
		Assert.assertArrayEquals(inicial, proyeccion.getTablero());
	}

	@Test
	public void evolucionarEstructuraOscilante() {
		EstadoCelula[][] inicial = {
				{MUERTA, MUERTA, MUERTA, MUERTA, MUERTA},
				{MUERTA, MUERTA, MUERTA, MUERTA, MUERTA},
				{MUERTA,   VIVA,   VIVA,   VIVA, MUERTA},
				{MUERTA, MUERTA, MUERTA, MUERTA, MUERTA},
				{MUERTA, MUERTA, MUERTA, MUERTA, MUERTA}
			};

		EstadoCelula[][] resultadoImpar = {
				{MUERTA, MUERTA, MUERTA, MUERTA, MUERTA},
				{MUERTA, MUERTA,   VIVA, MUERTA, MUERTA},
				{MUERTA, MUERTA,   VIVA, MUERTA, MUERTA},
				{MUERTA, MUERTA,   VIVA, MUERTA, MUERTA},
				{MUERTA, MUERTA, MUERTA, MUERTA, MUERTA}
			};

		JuegoDeLaVida juego = new JuegoDeLaVida(new Tablero(inicial));
		Tablero proyeccion = juego.proyectar(1);
		Assert.assertArrayEquals(resultadoImpar, proyeccion.getTablero());

		proyeccion = juego.proyectar(2);
		Assert.assertArrayEquals(inicial, proyeccion.getTablero());

		proyeccion = juego.proyectar(3);
		Assert.assertArrayEquals(resultadoImpar, proyeccion.getTablero());
	}

}
