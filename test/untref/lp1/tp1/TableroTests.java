package untref.lp1.tp1;

import static untref.lp1.tp1.EstadoCelula.MUERTA;
import static untref.lp1.tp1.EstadoCelula.VIVA;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class TableroTests {

	Tablero tablero;

	@Before
	public void setUp() {
		tablero = new Tablero(new EstadoCelula[][] {
					{ MUERTA, MUERTA, MUERTA },
					{ MUERTA,   VIVA, MUERTA },
					{ MUERTA, MUERTA, MUERTA }
				});
	}

	@Test
	public void queSabeInspeccionarPosicion() {
		Assert.assertEquals(MUERTA, tablero.inspeccionar(0, 0));
		Assert.assertEquals(VIVA, tablero.inspeccionar(1, 1));
	}

	@Test
	public void queSabeEvolucionarUnaGeneracionMuerta() {
		EstadoCelula[][] resultado = {
				{MUERTA, MUERTA, MUERTA},
				{MUERTA, MUERTA, MUERTA},
				{MUERTA, MUERTA, MUERTA}
			};

		JuegoDeLaVida juego = new JuegoDeLaVida(tablero);
		Tablero a = juego.proyectar(1);
		Assert.assertArrayEquals(resultado, a.getTablero());
	}

	@Test
	public void queSabeEvolucionarUnaGeneracionEstable() {
		EstadoCelula[][] inicial = {
				{MUERTA,   VIVA,   VIVA},
				{MUERTA,   VIVA,   VIVA},
				{MUERTA, MUERTA, MUERTA}
			};

		EstadoCelula[][] resultado = {
				{MUERTA,   VIVA,   VIVA},
				{MUERTA,   VIVA,   VIVA},
				{MUERTA, MUERTA, MUERTA}
			};

		JuegoDeLaVida juego = new JuegoDeLaVida(new Tablero(inicial));
		Tablero a = juego.proyectar(1);
		Assert.assertArrayEquals(resultado, a.getTablero());
	}

	@Test
	public void queSabeEvolucionarDiezGeneracionesEstable() {
		EstadoCelula[][] inicial = {
				{MUERTA,   VIVA,   VIVA},
				{MUERTA,   VIVA,   VIVA},
				{MUERTA, MUERTA, MUERTA}
			};

		EstadoCelula[][] resultado = {
				{MUERTA,   VIVA,   VIVA},
				{MUERTA,   VIVA,   VIVA},
				{MUERTA, MUERTA, MUERTA}
			};

		JuegoDeLaVida juego = new JuegoDeLaVida(new Tablero(inicial));
		Tablero a = juego.proyectar(10);
		Assert.assertArrayEquals(resultado, a.getTablero());
	}

	@Test
	public void queSabeEvolucionarUnTableroConTodasVivas() {
		EstadoCelula[][] inicial = {
				{  VIVA,   VIVA,   VIVA,   VIVA,   VIVA,   VIVA,   VIVA,   VIVA,   VIVA,   VIVA},
				{  VIVA,   VIVA,   VIVA,   VIVA,   VIVA,   VIVA,   VIVA,   VIVA,   VIVA,   VIVA},
				{  VIVA,   VIVA,   VIVA,   VIVA,   VIVA,   VIVA,   VIVA,   VIVA,   VIVA,   VIVA},
				{  VIVA,   VIVA,   VIVA,   VIVA,   VIVA,   VIVA,   VIVA,   VIVA,   VIVA,   VIVA},
				{  VIVA,   VIVA,   VIVA,   VIVA,   VIVA,   VIVA,   VIVA,   VIVA,   VIVA,   VIVA},
				{  VIVA,   VIVA,   VIVA,   VIVA,   VIVA,   VIVA,   VIVA,   VIVA,   VIVA,   VIVA},
				{  VIVA,   VIVA,   VIVA,   VIVA,   VIVA,   VIVA,   VIVA,   VIVA,   VIVA,   VIVA},
				{  VIVA,   VIVA,   VIVA,   VIVA,   VIVA,   VIVA,   VIVA,   VIVA,   VIVA,   VIVA},
				{  VIVA,   VIVA,   VIVA,   VIVA,   VIVA,   VIVA,   VIVA,   VIVA,   VIVA,   VIVA},
				{  VIVA,   VIVA,   VIVA,   VIVA,   VIVA,   VIVA,   VIVA,   VIVA,   VIVA,   VIVA}
			};

		EstadoCelula[][] resultado = {
				{MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA},
				{MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA},
				{MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA},
				{MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA},
				{MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA},
				{MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA},
				{MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA},
				{MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA},
				{MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA},
				{MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA}
			};

		JuegoDeLaVida juego = new JuegoDeLaVida(new Tablero(inicial));
		Tablero a = juego.proyectar(1);
		Assert.assertArrayEquals(resultado, a.getTablero());
	}

	@Test
	public void queSabeEvolucionarUnGliderEn16Generaciones() {
		EstadoCelula[][] inicial = {
				{MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA},
				{MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA},
				{MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA},
				{MUERTA, MUERTA, MUERTA, MUERTA,   VIVA, MUERTA, MUERTA, MUERTA, MUERTA},
				{MUERTA, MUERTA, MUERTA, MUERTA, MUERTA,   VIVA, MUERTA, MUERTA, MUERTA},
				{MUERTA, MUERTA, MUERTA,   VIVA,   VIVA,   VIVA, MUERTA, MUERTA, MUERTA},
				{MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA},
				{MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA},
				{MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA}
			};

		EstadoCelula[][] resultado = {
				{  VIVA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA,   VIVA,   VIVA},
				{MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA},
				{MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA},
				{MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA},
				{MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA},
				{MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA},
				{MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA},
				{MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA,   VIVA},
				{  VIVA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA}
			};

		JuegoDeLaVida juego = new JuegoDeLaVida(new Tablero(inicial));
		Tablero a = juego.proyectar(16);
		Assert.assertArrayEquals(resultado, a.getTablero());
	}

	@Test
	public void queSabeEvolucionarUnTableroDeUnaCelda() {
		EstadoCelula[][] inicial = {
				{VIVA}
			};

		EstadoCelula[][] resultado = {
				{MUERTA}
			};

		JuegoDeLaVida juego = new JuegoDeLaVida(new Tablero(inicial));
		Tablero a = juego.proyectar(1);
		Assert.assertArrayEquals(resultado, a.getTablero());
	}

	@Test
	public void queSabeEvolucionarVariasGeneracionesDeUnaPoblacionCualquiera() {
		EstadoCelula[][] inicial = {
				{MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA},
				{MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA},
				{MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA},
				{MUERTA, MUERTA, MUERTA,   VIVA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA},
				{MUERTA, MUERTA, MUERTA, MUERTA, MUERTA,   VIVA, MUERTA, MUERTA, MUERTA, MUERTA},
				{MUERTA, MUERTA,   VIVA,   VIVA, MUERTA, MUERTA,   VIVA,   VIVA,   VIVA, MUERTA},
				{MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA},
				{MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA},
				{MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA},
				{MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA}
			};

		EstadoCelula[][] resultado = {
				{MUERTA,   VIVA, MUERTA,   VIVA,   VIVA, MUERTA, MUERTA, MUERTA, MUERTA,   VIVA},
				{MUERTA,   VIVA,   VIVA,   VIVA,   VIVA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA},
				{MUERTA,   VIVA, MUERTA, MUERTA, MUERTA,   VIVA,   VIVA, MUERTA, MUERTA, MUERTA},
				{  VIVA,   VIVA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA},
				{MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA,   VIVA, MUERTA, MUERTA, MUERTA},
				{MUERTA, MUERTA, MUERTA, MUERTA,   VIVA, MUERTA,   VIVA, MUERTA, MUERTA, MUERTA},
				{MUERTA,   VIVA,   VIVA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA,   VIVA},
				{MUERTA, MUERTA,   VIVA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA, MUERTA,   VIVA},
				{MUERTA, MUERTA,   VIVA, MUERTA, MUERTA, MUERTA, MUERTA,   VIVA, MUERTA, MUERTA},
				{  VIVA, MUERTA, MUERTA, MUERTA, MUERTA,   VIVA, MUERTA, MUERTA,   VIVA,   VIVA}
			};

		JuegoDeLaVida juego = new JuegoDeLaVida(new Tablero(inicial));
		Tablero a = juego.proyectar(25);
		Assert.assertArrayEquals(resultado, a.getTablero());
	}

}
