package untref.lp1.tp1;

public enum EstadoCelula {
	VIVA, MUERTA;

	public String toString() {
		return this.name();
	}
}
