package untref.lp1.tp1;

/**
 * Una célula muerta con exactamente 3 células vecinas vivas nace o vive en el
 * siguiente turno. Una célula viva con 2 o 3 vecinas vivas, sigue viva. Una
 * célula muere por superpoblación si tiene 4 o más vecinos Una célula muere por
 * soledad si tiene 1 vecino o ninguno.
 * 
 */
public class JuegoDeLaVida {

	private Tablero tablero;

	public JuegoDeLaVida(Tablero tableroInicial) {
		this.tablero = tableroInicial;
	}

	public Tablero proyectar(Integer generaciones) {
		Tablero futuro = this.tablero;
		for (int i = 0; i < generaciones; i++) {
			futuro = proyectarUnaGeneracion(futuro.getTablero());
		}
		return futuro;
	}

	private Tablero proyectarUnaGeneracion(EstadoCelula[][] actual) {
		EstadoCelula[][] futuro = new EstadoCelula[actual.length][actual[0].length];
		for (int i = 0; i < actual.length; i++) {
			for (int j = 0; j < actual[i].length; j++) {
				futuro[i][j] = calcularNuevoEstado(actual[i][j],
						obtenerVecinos(i, j, actual));
			}
		}
		return new Tablero(futuro);
	}

	private EstadoCelula calcularNuevoEstado(EstadoCelula estadoCelula,
			EstadoCelula[] vecinos) {
		int celulasVivas = 0;

		for (EstadoCelula estado : vecinos) {
			if (EstadoCelula.VIVA == estado) {
				celulasVivas++;
			}
		}

		if (EstadoCelula.VIVA == estadoCelula) {
			return (celulasVivas == 2 || celulasVivas == 3) ? EstadoCelula.VIVA
					: EstadoCelula.MUERTA;
		} else {
			return celulasVivas == 3 ? EstadoCelula.VIVA : EstadoCelula.MUERTA;
		}
	}

	private EstadoCelula[] obtenerVecinos(int i, int j, EstadoCelula[][] actual) {
		EstadoCelula[] vecinos = new EstadoCelula[8];

		int fila = i;
		int columna = j;
		int filaAnterior = (fila == 0) ? actual.length - 1 : i - 1;
		int filaSiguiente = (fila == actual.length - 1) ? 0 : i + 1;
		int columnaAnterior = (columna == 0) ? actual[0].length - 1 : j - 1;
		int columnaSiguiente = (columna == actual[0].length - 1) ? 0 : j + 1;

		vecinos[7] = actual[filaAnterior][columnaAnterior];
		vecinos[6] = actual[filaAnterior][columna];
		vecinos[5] = actual[filaAnterior][columnaSiguiente];
		vecinos[4] = actual[fila][columnaAnterior];
		vecinos[3] = actual[fila][columnaSiguiente];
		vecinos[2] = actual[filaSiguiente][columnaAnterior];
		vecinos[1] = actual[filaSiguiente][columna];
		vecinos[0] = actual[filaSiguiente][columnaSiguiente];

		return vecinos;
	}

}
