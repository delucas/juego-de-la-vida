package untref.lp1.tp1;

public class Tablero {	

    private EstadoCelula[][] tablero;

	public Tablero(EstadoCelula[][] tablero) {
    	this.tablero = tablero;
    }

    public EstadoCelula inspeccionar(Integer posicionX, Integer posicionY) {
    	return tablero[posicionX][posicionY];
    }
    
    public EstadoCelula[][] getTablero() {
		return tablero;
	} 

}
